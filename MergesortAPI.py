from flask import Flask
from flask import request
app = Flask(__name__)

def merge(kiri, kanan):
  hasil = []
  nL = int(len(kiri))
  nR = int(len(kanan))
  i = 0
  j = 0
  while i < nL and j < nR:
    if kiri[i] <= kanan[j]:
      hasil.append(kiri[i])
      i += 1
    else:
      hasil.append(kanan[j])
      j += 1
  while i < nL:
    hasil.append(kiri[i])
    i += 1
  while j < nR:
    hasil.append(kanan[j])
    j += 1
  return hasil

def mergesort(list):
  n = int(len(list))
  if n > 1:
    branch_kiri = list[:int(n/2)]
    branch_kanan = list[int(n/2):]
    branch_kiri = mergesort(branch_kiri)
    branch_kanan = mergesort(branch_kanan)
    return merge(branch_kiri, branch_kanan)
  else:
      return list

@app.route('/mergesort')
def mergesorting():
    daftar = request.get_json()['Daftar']
    x = mergesort(daftar)
    return {'Hasil sorting' : x}





