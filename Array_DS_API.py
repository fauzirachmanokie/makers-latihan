from Array_DS import reverseArray
from flask import Flask
from flask import request
app = Flask(__name__)

@app.route('/Array_DS')
def array_ds():
    daftar = request.get_json()['array']
    x = reverseArray(daftar)
    return {'Hasil Reverse Array' : x}


