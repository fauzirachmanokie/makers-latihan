from flask import Flask
from flask import request
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/hello/<name>')
def nama(name: str):
  return{"msg": name}

#@app.route('/timeconversion/<s>')
#def timeConversion(s):
#    if s[8:] == 'PM' and s[:2] != '12':
#        s = str(int(s[:2])+12)+':'+s[3:5]+":"+s[6:8]
 #  if s[8:] == 'AM' and s[:2] == '12':
  #      s = '00:'+s[3:5]+":"+s[6:8]
 #   if s[8:] == 'PM' and s[:2] == '12':
  #      s = s[:8]
  #  else:
  #      s = s[:8]
  #  user = request.args.get('user')
  #  return {"Time Format Standard" : s, "Value" : user}

@app.route('/konversiwaktu/<s>', methods=['GET', 'POST'])
def timeconversion(s):
    if request.method == 'POST':
        if s[8:] == 'PM' and s[:2] != '12':
            s = str(int(s[:2])+12)+':'+s[3:5]+":"+s[6:8]
        if s[8:] == 'AM' and s[:2] == '12':
            s = '00:'+s[3:5]+":"+s[6:8]
        if s[8:] == 'PM' and s[:2] == '12':
            s = s[:8]
        else:
            s = s[:8]
        user = request.args.get('user')
        return {"Time Format Standard" : s, "Value" : user}
    else:
        return {"Format Waktu Awal" : s}

@app.route('/binary_search/<target>', methods=["POST"])
def binary_search(target):
    list =  request.get_json()['data']
    list.sort()
    while len(list) >=1:
        x = int(len(list)/2)
        if list[x] == int(target):
            return{'target sudah ditemukan ' : list[x]}
            break
        if list[x]> int(target):
            list = list[:x]
        elif list[x] < int(target):
            list = list[x:]

@app.route('/drawingbook')
def drawingbook():
    n = int(request.get_json()["n"])
    p = int(request.get_json()["p"])
    if n - p >= p:
        jump = int(p / 2)
    if n - p < p and p % 2 == 0:
        jump = int((n - p) / 2)
    elif n - p < p and p % 2 != 0 and n % 2 == 0:
        jump = int(((n - p) / 2) + 1)
    elif n - p < p and p % 2 != 0 and n % 2 != 0:
        jump = int((n - p) / 2)
    return {"jumlah halaman yang perlu dibalik" : jump}

@app.route('/kangaroo', methods=['POST'])
def kangaroo():
    kgr = request.get_json()['Kangaroo']
    x1 = kgr[0]
    x2 = kgr[2]
    v1 = kgr[1]
    v2 = kgr[3]
    if v2 - v1 == 0:
        chor = 'NO'
    elif ((x1 - x2) % (v2 - v1)) == 0 and ((x1 - x2) / (v2 - v1)) >= 0:
        chor = 'YES'
    else:
        chor = 'NO'
    return {"Apakah koreagrafinya berhasil": chor}



