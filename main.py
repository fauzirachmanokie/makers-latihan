from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(q: Optional[str], item_id: int):
    return {"item_id": item_id, "q": q}

@app.get("/hello/{name}")
def read_item(name: str):
    return {"msg": "hello " + name +"!"}

@app.get("/timeconversion/{s}")
def timeConversion(s: str):
    if s[8:] == 'PM' and s[:2] != '12':
         s = str(int(s[:2])+12)+':'+s[3:5]+":"+s[6:8]
    if s[8:] == 'AM' and s[:2] == '12':
        s = '00:'+s[3:5]+":"+s[6:8]
    if s[8:] == 'PM' and s[:2] == '12':
        s = s[:8]
    else:
        s = s[:8]
    return {"Time" : s}